%===============================================================================
\section{Experimental Results}
\label{sec:experimental_results}

%\subsection{Implementation} 
We tested Algorithm~\ref{alg:auto_nav_fastron_ogm} on a Racecar ground robot~\cite{karaman2017_racecar} in both simulation (Section~\ref{subsubsec:map_compare}) and real experiments (Section~\ref{sec:real_experiments}). The online training data and support vectors for the kernel-based mapping algorithm were generated on a grid $\bar{\mathcal{C}}$ with resolution $0.25m$. An eight-connected grid of resolution $0.1m$ was used for $A^*$ path planning. The stage cost was $c(x,u) := \|u\|$, where $u$ is one of the eight control inputs in 2D. During the map updates (Algorithm~\ref{alg:fastron_model}), the computation of the score function $F(x)$ was approximated using only $K$ nearest neighbors. This approximation is reasonable because the RBF kernel $k(x',x)$ approaches $0$ quickly as the distance $\|x' - x\|$ increases. The support vectors were stored in an $R^*$-tree data structure~\cite{beckmann1990r}, which allows efficient nearest-neighbor look up. We used $K=10$ in simulation and $K=3$ on the real robot.

%The grid resolution for  $\bar{\mathcal{C}}$ is $0.25m$. For path planning, we use $A^*$ on a 2D grid with resolution $0.1m$ where a cell $x$ can potentially go to its 8 neighbors $x_i, i = 1, \ldots, 8$ using 8 control signal $u_i = x_i - x$. The cost function is the Euclidean distance between $x$ and $x_i$: $c(x,u_i) = \Vert u_i \Vert$. The robot dynamics is $x_{t+1} = f(x_t,u_t) = x_t + u_t$ as mentioned in Section \ref{subsec:auto_nav}. In $A^*$, as described in Algorithm \ref{alg:get_successors}, the line segment is checked for collision using Algorithm \ref{alg:collision_checking_line} with Equation \eqref{eq:line_curve_t_condition}.

%In Eq. \eqref{eq:fastron_score}, since the RBF kernel $k(x_l,x)$ approaches $0$ quickly as the distance $\|x_l - x\|$ increases, only $K$ nearest support vectors are used to calculate the score $F(x)$. To find $K$ nearest support vectors, we store them in a $R^*$-tree data structure \cite{beckmann1990r} which provides efficient lookup times. For experiments, we use $K=10$ for simulation and $K=3$ on the real robot.



\subsection{Simulations}
\label{subsubsec:map_compare}
Our sparse kernel-based mapping method was compared with gmapping~\cite{gmapping}, a baseline occupancy grid mapping algorithm, in order to evaluate the reduction in memory required to store an occupancy map. A simulated warehouse environment, whose ground truth map is shown in Fig.~\ref{fig:simulation_results}, was used for the comparison. Since the ground-truth map represents the work space instead of C-space, we used a point robot ($r=0$) for an accurate comparison. Simulated lidar scan measurements were collected along a robot trajectory shown in Fig.~\ref{fig:simulation_results}. As the robot moved in the environment, it ran our mapping method and gmapping simultaneously. Table~\ref{table:accuracy} compares the accuracy of the proposed kernel-based map, the inflated map (according to the upper bound in Proposition~\ref{prop:score_bounds}), and gmapping. The support vectors maintained by our kernel-based map are shown in Fig.~\ref{fig:simulation_results}. The actual kernel-based map as well as the maps from the upper bound on the perceptron score and gmapping are shown in Appendix~\ref{subsubsec:map_comparison_appendix}. Kernel-based mapping and gmapping lead to an equivalent accuracy of $\sim 95\%$ compared to the ground truth map (errors are due to lidar noise). \textit{However, our map stores $\boldsymbol{10}$ times less data to achieve the same representation accuracy as gmapping}. The number of support vectors over time maintained by our algorithm is compared to the number of observed cells maintained by gmapping in Fig.~\ref{fig:support_vec_count}. Both numbers are increasing as the robot is exploring the environment and become constant as the whole environment is observed (around time step $12000$). The rate of increase of the data maintained by our algorithm is significantly lower than that of gmapping. Appendix~\ref{subsubsec:robot_safety_appendix} shows that, despite a very sparse representation, our map correctly evaluates the safety of robot trajectories in C-space.

\subsection{Robot Safety}
\label{subsec:robot_safety_appendix}
In this section, we use a simulated model of the Racecar ground robot \cite{karaman2017_racecar}. The shape of the simulated robot is bounded by a ball of radius $r = 0.25$. As mentioned in Section \ref{sec:problem_formulation}, we take the robot's shape into account by formulating the navigation problem in the C-space. To verify that the robot does not crash into obstacles in the environment, we calculate the distance from the robot to the closest occupied cell in the ground truth map while navigating from a start to a goal. The robot's trajectory is shown in Fig. \ref{fig:safe_path_distance}(left) and its distance to the closest obstacle along the trajectory is plotted in Fig. \ref{fig:safe_path_distance} (right). It shows that the distance is always larger than the robot's radius which verify the robot's safety.

\begin{figure}[h!]
\centering
\includegraphics[width=0.45\textwidth]{fig/safepath_experiment.pdf} \hfill
\includegraphics[width=0.45\textwidth]{fig/distance_safe_path.pdf}
\caption{Robot safety on a path planned by $A^*$: robot path (left) and distance to the closest obstacle (right).}
\label{fig:safe_path_distance}
\end{figure}


% The support vectors maintained by our algorithm are shown in Fig.~\ref{fig:support_vec_plot}. 


% and the ground truth map of the same resolution in simulation. The environment is a simulated warehouse whose ground truth map is shown in Fig. \ref{fig:ground_truth}. As the ground truth map of the environment is in the work space instead of the C-space, we use a point robot (i.e. $r=0$) to build the map in the work space for a fair comparison. We pick different goals iteratively so that the robot can explore the environment and build the map using Algorithm \ref{alg:auto_nav_fastron_ogm}. Fig. \ref{fig:ground_truth} also shows the robot trajectory. While the robot moves in the environment, it performs our mapping method and gmapping simultaneously. Our map and the ground truth map look similar as shown in Fig. \ref{fig:map_comparison}. Table \ref{table:accuracy} shows our map and gmapping's are $\sim 98\%$ the same and both achieve $\sim 95\%$ accuracy compared to the ground truth. The maps generated by gmapping and by our upper bound in Proposition \ref{prop:score_bounds} are shown in Appendix \ref{subsec:add_results}. The upper bound inflates our map in Fig. \ref{fig:fastron_map} for efficient collision checking and, as a result, increases the robot's safety in navigation. However, it sacrifices $\sim 15\%$ accuracy compared to the ground truth map as shown in Table \ref{table:accuracy}.

%Fig. \ref{fig:support_vec_plot} plots the support vectors that generates our map. Fig. \ref{fig:support_vec_count} shows the number of support vectors stored by our map and the number of observed cells stored by gmapping. Both numbers are increasing as the robot navigates through the environment. Around time step $12000$, the robot finishes building the map as the number of support vectors and that of cells observed by gmapping stay constant. After the map is built, our map only needs to store the support vectors which takes $\boldsymbol{\sim 10}$ times less than gmapping does while maintaining similar accuracy.

%To verify the robot safety as we plan the trajectory in the C-space, we use a simulated model of the Racecar ground robot \cite{karaman2017_racecar} and show that the robot does not collides with any obstacles navigating from a start to a goal in Appendix \ref{subsec:add_results}.

%\begin{figure}[t]
%\centering
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.40in]{fig/groundtruth_map_path.pdf}
%        \caption{Ground truth map and robot trajectory.}
%        \label{fig:ground_truth}
%\end{subfigure}%
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.40in]{fig/fastron_map.pdf}
%        \caption{Our kernel-based map.}
%        \label{fig:fastron_map}
%\end{subfigure}%


%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.40in]{fig/support_vec.pdf}
%        \caption{Support vectors (red and blue points).}
%        \label{fig:support_vec_plot}
%\end{subfigure}%
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.40in]{fig/support_vec_count.pdf}
%        \caption{Number of points stored by our map and gmapping}
%        \label{fig:support_vec_count}
%\end{subfigure}%
%\caption{Training an augmented dataset from laser scans using Fastron algorithm.}
%\label{fig:map_comparison}
%\end{figure}


\begin{figure}[h!]
\centering
\begin{subfigure}[h!]{0.5\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{fig/groundtruth_map_path.pdf}
        \caption{Ground truth map and robot trajectory.}
        \label{fig:ground_truth}
\end{subfigure}%

\begin{subfigure}[h!]{0.5\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{fig/fastron_map.pdf}
        \caption{Our kernel-based map.}
        \label{fig:fastron_map}
\end{subfigure}%

\begin{subfigure}[h!]{0.5\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{fig/gmap.pdf}
        \caption{Occupancy grid from Gmapping~\cite{gmapping}.}
        \label{fig:gmapping}
\end{subfigure}%

\begin{subfigure}[h!]{0.5\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{fig/fastron_upperbound.pdf}
        \caption{Inflated map generated using the upper bound $U(x)$ proposed in Proposition ~\ref{prop:score_bounds}.}
        \label{fig:upperbound_map}
\end{subfigure}%
\caption{Ground truth map and robot trajectory used to generate simulated lidar scans and results from our algorithms and gmapping.}
\label{fig:map_comparison}
\end{figure}

\begin{table}
\centering
\begin{tabular}{ |c|c|c|c|c| } 
 \hline
 & KM& IM &GM & GT\\
 \hline
 KM & 100\% & 80.08\% & 98.12\% & 95.74\% \\
 \hline
 IM & 80.08\% & 100\% & 81.42\% & 79.94\% \\
 \hline
 GM & 98.12\% & 81.42\%& 100\% & 95.54\% \\ 
 \hline
 GT & 95.74\% & 79.94\%&95.54\% & 100\%\\  
 \hline
\end{tabular}
\caption{Agreement among our kernel-based map (KM), our inflated map (IM), the gmapping map (GM)~\cite{gmapping}, and the ground truth map (GT) obtained from simulated lidar data.}
\label{table:accuracy}
\end{table}


\begin{figure}[t]
\includegraphics[width=\linewidth]{fig/support_vec.pdf}

\includegraphics[width=\linewidth]{fig/support_vec_count.pdf}
\caption{Support vectors (red and blue points) (top) and number of points stored by our kernel-based map and gmapping (bottom).}
\label{fig:support_vec_count}
\end{figure}

\begin{figure}[t]
\centering
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/cam1.jpg}
        %\caption{At time $t=0s$}
        %\label{fig:cam1}
\end{subfigure}%
\hfill%
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/cam3.jpg}
        %\caption{At time $t=110s$}
        %\label{fig:cam3}
\end{subfigure}%
\hfill%
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/zed1.jpg}
        \caption{Time $t=0s$}
        \label{fig:zed1}
\end{subfigure}%
\hfill%
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/zed3.jpg}
        \caption{Time $t=110s$}
        \label{fig:zed3}
\end{subfigure}%


\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/cam5.jpg}
        %\caption{At time $t=270s$}
        %\label{fig:cam5}
\end{subfigure}%
\hfill%
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/cam6.jpg}
        %\caption{At time $t=350s$}
        %\label{fig:cam6}
\end{subfigure}%
\hfill%
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/zed5.jpg}
        \caption{Time $t=270s$}
        \label{fig:zed5}
\end{subfigure}%
\hfill%
\begin{subfigure}[t]{0.24\textwidth}
        \centering
        \includegraphics[width=\linewidth]{fig/zed6.jpg}
        \caption{Time $t=350s$}
        \label{fig:zed6}
\end{subfigure}%
\caption{Images from a third person view (top row) and from an onboard camera (bottom row).}
\label{fig:camera_images}
\end{figure}

%\begin{table}[t]
%\caption{Agreement among our kernel-based map (KM), our inflated map (IM), the map from gmapping~\cite{gmapping} (GM), and the ground truth map (GT) generated using simulated lidar data.}
%\label{table:accuracy}
%\begin{tabular}{ |c|c|c|c|c| } 
% \hline
% & KM& IM &GM & GT\\
% \hline
% KM & 100\% & 80.08 \% & 98.12\% & 95.74\% \\
% \hline
% IM & 80.08\% & 100\% & 81.42\% & 79.94\% \\
% \hline
% GM & 98.12\% & 81.42\%& 100\% & 95.54\% \\ 
% \hline
% GT & 95.74\% & 79.94\%&95.54\% & 100\%\\  
% \hline
%\end{tabular}
%\end{table}



%\begin{figure}[t]
%\centering
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.45in]{fig/support_vec.pdf}
%        \caption{Support vectors}
%        \label{fig:support_vec}
%\end{subfigure}%
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.45in]{fig/support_vec_count.pdf}
%        \caption{Cell count}
%        \label{fig:support_vec_count}
%\end{subfigure}%
%        \caption{The number of support vectors versus the number of cells observed and stored by gmapping.\TODO{I suggest removing the subfigures to save space. See below for an example.}}
%\label{fig:support_vec_plot}
%\end{figure}
%\begin{figure}[t]
%\centering
%\includegraphics[width=0.49\linewidth]{fig/support_vec.pdf}\hfill%
%\includegraphics[width=0.5\linewidth]{fig/support_vec_count.pdf}
%\caption{Support vectors (red and blue points) generated by our kernel-based mapping algorithm (left). The number of stored support vectors over time is compared to the number of observed and stored cells by an occupancy grid mapping algorithm (right).}
%\label{fig:support_vec_plot}
%\end{figure}
\subsection{Real Robot Experiments}
\label{sec:real_experiments}
%\begin{figure}[t]
%\centering
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.8in]{fig/realcar_supportvec.pdf}
%        \caption{Support vectors maintained by our algorithm}
%        \label{fig:realcar_support_vec}
%\end{subfigure}%
%\begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[height=1.8in]{fig/realcar_fastronmap.pdf}
%        \caption{Our kernel-based map}
%        \label{fig:realcar_final_map}
%\end{subfigure}%
%\caption{Our map of the environment}
%\label{fig:finalmap_supportvec}
%\end{figure}
\begin{figure}[t]
\begin{minipage}{\linewidth}
\includegraphics[width=\linewidth]{fig/realcar_supportvec.pdf}
\caption{Support vectors maintained by our algorithm}
\label{fig:realcar_support_vec}
\end{minipage}%

\begin{minipage}{\linewidth}
\includegraphics[width=\linewidth]{fig/realcar_fastronmap.pdf}
\caption{Kernel-based occupancy map}
\label{fig:realcar_final_map}
\end{minipage}
\end{figure}
\begin{figure}[h!]
\centering
\includegraphics[width=\linewidth]{fig/realcar_supportvec_size.pdf}%

\includegraphics[width=\linewidth]{fig/realcar_astartime_pernode.pdf}%

\includegraphics[width=\linewidth]{fig/realcar_map_update.pdf}
\vspace{-2mm}
\caption{Number of support vectors over time (left), $A^*$ planning time per expaned node (middle), and map update time (right).}
\label{fig:veccount_astar_map_update_time}
\vspace{-7mm}
\end{figure}
We tested our algorithm on an ackermann-drive Racecar robot equipped with a Hokuyo UST-10LX lidar and Nvidia TX1 computer. The robot body is bounded by a ball of radius $r = 0.3m$. The robot navigated an unknown office environment to destinations randomly chosen by a human operator. Fig. \ref{fig:camera_images} shows the images from a third person view and from a camera onboard the robot as it navigates through the environment. Fig.~\ref{fig:realcar_support_vec} and Fig.~\ref{fig:realcar_final_map} show the support vectors and the kernel-based map generated in the real experiment. During the experiment, the robot was directed to make a loop and revisit previously observed regions. As a result, the number of support vectors increases at first but then becomes constant as the robot turns around as seen in Fig.~\ref{fig:veccount_astar_map_update_time}. The inflated map obtained from the upper bound $U(x)$ in Proposition.~\ref{prop:score_bounds} is shown in Appendix \ref{subsubsec:real_car_appendix}. During the real experiment, we noticed that our mapping method is susceptible to noise since the support vectors are quickly updated with newly observed data, even if the labels are noisy or affected by localization errors. The underlying reason is that kernel perceptron is a discriminative model and does not model the occupancy probability of the environment.

The map update time (the time taken by Algorithm~\ref{alg:fastron_model} to update the support vectors from one lidar scan) and the $A^*$ replanning time are shown in Fig.~\ref{fig:veccount_astar_map_update_time}. It takes at most $3.5$ seconds to update the map onboard the real robot but our implementation is not optimized or parallelized. The replanning time is normalized by the number of expaneded nodes to account for the difference in planning to nearby and far goals. The planning time per expanded node increases over time as more support vectors are added to the map and used for collision checking. The planning time stabilizes when the whole environment is explored.


%In this section, we test our proposed method with a Racecar robot \TODO{Do we need to describe the hardware (CPU, GPU, RAM, etc.)?)}. The robot is bounded by a ball of radius $r \approx 0.3m$. The environment is an office area in our building. As mentioned in Section \ref{subsubsec:map_compare}, we also pick the goals iteratively so that the car can explore the environment using Algorithm \ref{alg:auto_nav_fastron_ogm}. Fig. \ref{fig:camera_images} shows the images from a third person view and from a camera onboard the robot as it navigates through the environment. Fig. \ref{fig:realcar_support_vec}, \ref{fig:realcar_final_map} shows the support vectors (red and blue points), and our kernel-based map, respectively. The robot's trajectory plotted in Fig. \ref{fig:realcar_final_map} shows that the robot turns around and revisits the previously observed regions. As a result, the number of support vectors increases at first but then becomes stable after the robot turns around as seen in Fig. \ref{fig:veccount_astar_map_update_time} (left). The inflated map based on the upper bound $U(x)$ in Proposition.~\ref{prop:score_bounds} is shown in Appendix \ref{subsec:add_results}. On the actual robot, we notice that our mapping method is prone to noise as Fastron algorithm quickly updates the support vectors with newly-observed labels even if the labels are from noisy observations or affected by localization errors. The underlying reason is that kernel perceptron is a discriminative model and does not provide occupancy probability. 

%%As the number of support vectors increases, it takes longer to check a line for collision. 
%Since the planning time is shorter as the robot is closer to the goal and, hence, expand fewer nodes, we calculate the planning time and normalize it by the number of node expanded by $A^*$ for a fair comparison. As plotted in Fig. \ref{fig:veccount_astar_map_update_time}(middle), the planning time per expanded node is increase over time as we have more support vectors and is stable after the map is built . We also show the map update time, i.e. the time taken by Fastron algorithm to update the support vectors from one observation, in Fig. \ref{fig:veccount_astar_map_update_time}(right). On our real robot, it take at most $3.5$ seconds to finish one update. Note that our implementation is not optimized yet as many components can be improved using parallel computation.






