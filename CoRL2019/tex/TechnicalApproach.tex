%===============================================================================
\iftrue
\section{Preliminaries}
\label{sec:prelim}
%In this section, we briefly describe the kernel perceptron model and Fastron~\cite{das2017fastron}, an efficient algorithm to train kernel perceptron models. %We also provide a short summary on occupancy grid map, a common map representation in robotics.
\textbf{Kernel Perceptron}. The kernel perceptron algorithm is used to classify a set of $N$ labeled data points. For $l = 1, \ldots, N$, a data point $x_l$ with label $y_l \in \{-1, 1\}$ is assigned a weight $\alpha_l$. The kernel perceptron training algorithm returns a set of $M^+$ positive support vectors and their weight $\Lambda^+ = \{(x_i, \alpha_i)\}$ and a set of $M^-$ negative support vectors and their weight $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$. A decision boundary is represented by a score function 
\begin{equation}
\label{eq:fastron_score}
F(x) =\sum_{i = 1}^{M^+} \alpha_i^+ k(x_i^+, x) - \sum_{j = 1}^{M^-} \alpha_j^- k(x_j^-, x), \quad \alpha_j^-, \alpha_i^+ > 0.
\end{equation}
where $k(\cdot, \cdot)$ is the kernel function. The sign of $F(x)$ is used to predict the class of $x$. During training, if $x_l$ is misclassified by the function $F(x)$, its weight $\alpha_l$ is updated by adding~$1$.

\textbf{Fastron}. Fastron is an efficient algorithm to train kernel perceptron models which was proposed for collision detection (Algorithm \ref{alg:fastron_model})\cite{das2017fastron, das2019learning}. It prioritizes updating misclassified points based on their margins instead of randomly running through the dataset (lines 4 \& 5). It is shown in \cite{das2017fastron, das2019learning} that if $\alpha_l = ry_l - (\sum_{i\neq l} \alpha^+_i k(x^+_i, x_l) - \sum_{j\neq l} \alpha^-_j k(x^-_j, x_l))$ for some $r > 0$, the point $x_i$ with label $y_l$ is correctly classified. Based on this fact, Fastron utilizes one-step weight correction ${\Delta \alpha = ry_l - (\sum \alpha^+_i k(x^+_i, x_l) - \sum \alpha^-_j k(x^-_j, x_l))}$ where $r = r^+$ if $y_l = 1$ and $r =r^-$ if $y_l = -1$ (lines 6 \& 7). \NEW{For dynamic environments, Fastron actively resamples in the proximity of the current support vectors to generate a dataset $\mathcal{D}$ and globally updates the support vectors. In our setting, we instead generate training data from our streaming partial depth observations at a time $t$ and update the support vectors locally}.
\begin{algorithm}
\caption{\NEW{Fastron \cite{das2019learning}}}
\label{alg:fastron_model}
  \footnotesize
	\begin{algorithmic}[1]
		\Require Set of $M^+$ positive support vectors and their weight $\Lambda^+ = \{(x^+_i, \alpha^+_i)\}$; set of $M^-$ positive support vectors and their weight $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$; training set $\mathcal{D} = \{(p_l, q_l)\}$; $r^+, r^- > 0$; $N_{max}$
		\Ensure Updated $\Lambda^+, \Lambda^-$.
		\For {$(p_l, q_l)$ in $\mathcal{D}$}
			\State Calculate $F_l = \sum_{i = 1}^{M^+} \alpha^+_i k(x^+_i,p_l) - \sum_{j = 1}^{M^-} \alpha^-_j k(x^-_j,p_l)$
		\EndFor
		\For {$t = 1$ to $N_{max}$}
			\If{$q_lF_l > 0 \quad \forall l$} \Return{$\Lambda^+, \Lambda^-$} \Comment{Margin-based priotization}
			\Else $\quad m \leftarrow \text{argmin}_l q_l F_l$	
			\EndIf
			\If{$q_m > 0$}  $\Delta\alpha \leftarrow r^+q_m -F_m$ \Comment{One-step weight correction}
			\Else $\quad\Delta\alpha \leftarrow r^-q_m -F_m$
			\EndIf
			\If{$\exists \alpha^+_m: (p_m, \alpha^+_m) \in \Lambda^+$}
			$\alpha^+_m \leftarrow \alpha^+_m + \Delta \alpha$, $F_i \leftarrow F_i + k(x_i, x_m) \Delta\alpha$ $\quad\forall i$
			\ElsIf{$\exists \alpha^-_m: (p_m, \alpha^-_m) \in \Lambda^-$}
			$\alpha^-_m \leftarrow \alpha^-_m - \Delta \alpha$, $F_i \leftarrow F_i - k(x_i, x_m) \Delta\alpha$ $\quad\forall i$
				\Else 
					\If{$q_m > 0$} $\alpha^+_m \leftarrow \Delta \alpha$, $\Lambda^+ \leftarrow \Lambda^+ \cup \{(p_m, \alpha^+_m)\}$
					\Else{$\quad\alpha^-_m \leftarrow -\Delta \alpha$, $\Lambda^- \leftarrow \Lambda^- \cup \{(p_m, \alpha^-_m)\}$}
				\EndIf
			\EndIf
			\For{$(p_l, q_l) \in \mathcal{D}$} \Comment{Remove redundant support vectors}
				\If{$\exists \alpha^+_l: (p_l, \alpha^+_l) \in \Lambda^+$ and $q_l(F_l - \alpha^+_l) > 0$}
				\State $\Lambda^+ \leftarrow \Lambda^+ \setminus \{(p_l, \alpha^+_l)\}, F_n \leftarrow F_n - k(x_l, x_n)\alpha^+_l\quad \forall (x_n, \cdot) \in \mathcal{D}$.
				\EndIf
				\If{$\exists \alpha^-_l: (p_l, \alpha^-_l) \in \Lambda^-$ and $q_l(F_l + \alpha^-_l) > 0$}
				\State $\Lambda^- \leftarrow \Lambda^- \setminus \{(p_l, \alpha^-_l)\}, F_n \leftarrow F_n + k(x_l, x_n)\alpha^-_l\quad \forall (x_n, \cdot) \in \mathcal{D}$.
				\EndIf
			\EndFor
		\EndFor
		\State \Return{$\Lambda^+, \Lambda^-$}
	\end{algorithmic}
\end{algorithm}

In the next section, we will describe our approach on integrating kernel perceptron into occupancy grid mapping to determine a cell's occupancy.
%===============================================================================
\section{Technical Approach} \label{sec:technical_approach}
%We present the map learning process using a kernel percetron model in Section \ref{subsec:ogm_with_fastron}, propose an efficient collision checking algorithm for the proposed map in Section~\ref{subsec:collison_checking_with_fastron_map}, and show how to perform motion planning in an unknown environment, thus solving Problem~\ref{problem_formulation_unknown_env} in Section~\ref{subsec:auto_nav}.
\subsection{Sparse Kernel-based Occupancy Mapping}
\label{subsec:ogm_with_fastron}
%
\NEW{We consider the scan from the depth sensor at time $t$ as our observation $z_t$.} Fig. \ref{fig:laser_scan_ws} demonstrates the lidar scan from a disk-shaped robot in the workspace. \NEW{Let $\bar{\mathcal{C}}$ denote a grid over the C-space, which will be used to pre-process the observation $z_t$}. In C-space, each endpoint of the laser rays is replaced by a disk-shaped obstacle of radius $r$, while the disk-shaped robot becomes a point robot as shown in Fig. \ref{fig:laser_scan_cs}. The lidar scan is sampled on the grid $\bar{\mathcal{C}}$ to generate a set $\bar{z}_t$ of data cells (Fig. \ref{fig:orig_data}) with label ``1" (occupied) if a cell is occupied by any of the disk-shaped obstacles and with label ``-1" (free), otherwise. As the unobserved cells are assumed to be free,  a neighboring cell of a sample is added to an augmented set $z'_t$ with label ``-1" if it belongs to neither $\bar{z}_t$ nor the current set of support vectors. The augmented dataset $z'_t$ is illustrated in Fig. \ref{fig:augmented_data} assuming the set of support vectors is empty. We obtain an training set $\mathcal{D} = \bar{z}_t \cup z'_t$ (Fig. \ref{fig:training_data} and Algorithm \ref{alg:augmented_data} in Appendix \ref{app:data_gen}) for training our kernel perceptron model using Fastron algorithm with the RBF kernel $k(x_i, x) = \eta\exp(-\gamma\Vert x_i - x \Vert^2)$. 
%Our goal is to generate a training set $\mathcal{D} = \{(p_i, q_i): p_i \in \bar{\mathcal{C}}, q_i \in \{-1, 1\}\}$ for \NEW{Fastron algorithm} from the observation $z_t$. 
%A grid point $p'$ is considered a neighbor of a grid point $p$ if $\Vert p - p' \Vert_\infty < \delta$ where $\delta$ is a pre-defined threshold.
%Since we assume the unobserved cells are free, we need to consider the neighboring cells of all the occupied cells in $\bar{z}_t$ to the dataset
%As mentioned in Section \ref{sec:prelim}, the Fastron algorithm requires labeled grid cells and their score $F(x)$ for the entire environment which makes it incompatible with our partial observation $z_t$. Also, storing the labeled cells and their score for the whole grid needs the same space complexity as storing an occupancy grid map. \TODO{To overcome this, we modify}\NA{rewrite -- avoid using modify} the Fastron algorithm such that it can incrementally take training data from a partial observation as input. Specifically, in Algorithm \ref{alg:fastron_model}, the training set $\mathcal{D}$ only contains labeled data generated from a partial observation $z_t$. The score $F(x)$ for the cells in $\mathcal{D}$ based on the current set of support vectors are re-calculated instead of being stored. As a result, the original Fastron requires $O(N)$ space where $N$ is the total number of cells on the grid $\bar{\mathcal{C}}$ while our \TODO{modified Fastron} only needs $O(M + |\mathcal{D}|)$ space where $M$ is the number of support vectors and the size of the training set $|\mathcal{D}|$ which is bounded by a constant.
The kernel hyperparameters can be optimized offline using automatic relevance determination~\cite{Rasmussen_2006} based on training data from a ground truth occupancy map. Let $N$ and $M = M^+ + M^- \ll N$ denote the total number of cells in $\bar{\mathcal{C}}$ and the total number of support vectors, respectively. As a result, storing $M$ support vectors consumes significantly less memory than storing the entire grid $\bar{\mathcal{C}}$. Based on the support vectors, we can represent the estimated map $\hat{m}_t$ by evaluating the score $F(x)$ in Eq. \eqref{eq:fastron_score}. \NEW{Specifically, $\hat{m}_t(x) = -1$ (free) if $F(x) < 0$ and $\hat{m}_t(x) = 1$ (occupied) if $F(x) \geq 0$}. Fig.\ref{fig:boundary_example}  illustrates the boundaries and the support vectors generated by Fastron for a grid map.

\begin{figure}[t]
%\vspace{-2mm}
\centering
\begin{subfigure}[t]{0.25\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/lidar_example.pdf}
        \caption{}
        \label{fig:laser_scan_ws}
\end{subfigure}%
\begin{subfigure}[t]{0.25\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/lidar_cspace_example.pdf}
        \caption{}
        \label{fig:laser_scan_cs}
\end{subfigure}%
\begin{subfigure}[t]{0.25\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/samples_from_lidar_orig.pdf}
        \caption{}
        \label{fig:orig_data}
\end{subfigure}%
\begin{subfigure}[t]{0.25\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/samples_from_lidar_augmented.pdf}
        \caption{}
        \label{fig:augmented_data}
\end{subfigure}%

\begin{subfigure}[t]{0.25\textwidth}
        \centering
        \includegraphics[width=\textwidth]{fig/samples_from_lidar.pdf}
        \caption{}
        \label{fig:training_data}
\end{subfigure}%
\begin{subfigure}[t]{0.25\textwidth}
        \centering
\includegraphics[width=\textwidth]{fig/upper_bound_example.pdf}%
        \caption{}
        \label{fig:boundary_example}
\end{subfigure}%
\begin{subfigure}[t]{0.25\textwidth}
        \centering
\includegraphics[width=0.97\textwidth]{fig/upper_bound_along_path1.pdf}%
        \caption{}
        \label{fig:upperbound_line1}
\end{subfigure}%
\begin{subfigure}[t]{0.25\textwidth}
        \centering
\includegraphics[width=\textwidth]{fig/upper_bound_along_path2.pdf}
        \caption{}
        \label{fig:upperbound_line2}
\end{subfigure}%
\vspace{-2mm}
\caption{Example of our mapping method: (a) scan in workspace; (b) scan in C-space; (c) samples from scan; (d) augmented free points (e) training data $\mathcal{D}$ from one lidar scan; (f) the exact decision boundary generated by the perceptron score $F(x)$ and the inflated boundary generated by the upper bound $U(x)$. The perceptron score and upper bound are compared along two rays, (g) one that enters the occupied space and (h) one that remains obstacle-free.}
\label{fig:upper_bound_along_path}
\vspace{-6mm}
\end{figure}
\subsection{Collision Checking with Kernel-based Occupancy Maps}
\label{subsec:collison_checking_with_fastron_map}
A map representation is useful for motion planning only if it allows checking a potential robot trajectory $s(t)$ for collisions. Sampling-based methods~\cite{Tsardoulias2016planninggridmap} only check a set of discrete samples on the trajectory and, hence, do not provide safety guarantees. In our map representation, checking that a complete curve $s(t)$ is collision-free is equivalent to verifying that $F(s(t)) < 0$ for all $t \geq 0$. It is not possible to express this condition for $t$ explicitly due to the nonlinearity of $F$. Instead, in Proposition \ref{prop:score_bounds}, we show that an accurate upper bound $U(s(t))$ of the score function $F(s(t))$ exists and can be used to evaluate the condition $U(s(t)) < 0$ explicitly in terms of $t$.
%A major advantage of our kernel-based occupancy map is that it models the robot's C-space directly. Hence, collision checking can be performed for a point robot. 
%An important aspect of a map representation is to allow collision checking for a given robot trajectory \NEW{$s(t)$} generated by a control signal (e.g. velocity, acceleration, or a motion primitive, etc.). \NEW{Sampling-based collision checking methods only check samples on the trajectory and, hence, does not provide any guarantees.} %\TODO{A common method is to sample the curve and check the sample points for collision using the perceptron score in Eq. \eqref{eq:fastron_score}. However, this sampling-based method cannot guarantee the entire curve is collision-free due to its sampling nature.}\NA{shorten}
%Another way to check the curve $s(t)$ for collision is to directly solve the inequality $F(s(t)) < 0$. This inequality cannot be solved in closed form. Instead, in Proposition \ref{prop:score_bounds}, we show that an accurate upper bound $U(s(t))$ of the score function $F(s(t))$ exists for which evaluating the condition $U(s(t)) \leq 0$ can be performed in closed form.

\begin{proposition}
\label{prop:score_bounds}
The score $F(x)$ is bounded above by $U(x) = k(x, x_*^+) \sum_{i = 1} ^ {M^+} \alpha^+_i  - k(x, x_j^-)\alpha^-_j$ where $x_{*}^{+}$ is the closest positive support vector to $x$ and $x_j^-$ is any negative support vector.
\end{proposition}
\vspace{-5mm}
\begin{proof}
The proposition holds because $k(x, x_i^+) = \eta\exp{(-\gamma\Vert x - x_i^+ \Vert^2)} \leq k(x, x_*^+) = \eta\exp{(-\gamma \Vert x - x_*^+ \Vert^2)}$ for all $x_i^+$ and $\sum_{j = 1}^{M^-} \alpha_j^- k(x, x_j^-) \geq \alpha_j^- k(x, x_j^-)$ for all $x_j^-$.
\vspace{-3mm}
\end{proof}
%,
%\[
%F(x) = \sum_{i = 1}^{M^+} \alpha_i^+ k(x_i^+, x) - \sum_{j = 1}^{M^-} \alpha_j^- k(x_j^-, x) \leq k(x, x_*^+) \sum_{i = 1} ^ {M^+} \alpha^+_i  - \alpha^-_j k(x, x_j^-) \qedhere
%\]
Fig.~\ref{fig:boundary_example}, \ref{fig:upperbound_line1}, and \ref{fig:upperbound_line2} illustrate the exact decision boundary $F(x) = 0$ and the accuracy of the upper bound $U(s(t))$ along two lines $s(t)$ in C-space. The upper bound is loose in the occupied space but remains close to the perceptron score in the free space. As a result, the boundary $U(x) = 0$ generated by the upper bound remains close to the true decision boundary. The reason is that the RBF kernel $k(x,x')$ approaches $0$ quickly as the robot moves away from the obstacle's boundary. The upper bound provides a conservative but reasonably accurate ``inflated boundary''. The inflated boundary allows efficient collision checking for line segments and polynomial curves as shown next.
%Since robot trajectories are planned in free space, t
%not only increases the robot's safety by effectively padding the actual obstacle boundary but also 

%\NEW{Fig. \ref{fig:upper_bound} shows the boundary generated by the upper bound $U(x)$ while Fig.~\ref{fig:upper_bound_along_path1} and Fig.~\ref{fig:upper_bound_along_path2} plot $U(x)$ along along two lines}. Along the lines, the upper bound is loose in the occupied region while staying close to the perceptron score in the free region. As a result, the inflated boundary is still close to the actual perceptron boundary. This is because the RBF kernel $k(x, x')$ approaches $0$ quickly as the robot moves away from the obstacles in the free region. Since we plan a path in the free region, the upper bound provides a conservative but reasonably accurate \NEW{``inflated boundary"} for collision checking. It increases the robot's safety during navigation by effectively padding the obstacle's boundary. Using the upper bound, we propose an efficient method to check a line segment or a polynomial curve for collision in the next sections.
%\begin{subfigure}[t]{0.333\textwidth}
%        \centering
%        \includegraphics[height=1.7in]{fig/upper_bound_example.pdf}
%        \caption{Perceptron boundary and inflated boundary}
%        \label{fig:upper_bound}
%\end{subfigure}%
%\begin{subfigure}[t]{0.333\textwidth}
%        \centering
%        \includegraphics[height=1.7in]{fig/upper_bound_along_path1.pdf}
%        \caption{The upper bound along line 1.}
%        \label{fig:upper_bound_along_path1}
%\end{subfigure}%
%\begin{subfigure}[t]{0.333\textwidth}
%        \centering
%        \includegraphics[height=1.7in]{fig/upper_bound_along_path2.pdf}
%        \caption{The upper bound along line 2.}
%        \label{fig:upper_bound_along_path2}
%\end{subfigure}%
\subsubsection{Collision Checking for Line Segments}
\label{subsubsec:collision_check_line}
Suppose that the robot's path is described by a ray $s(t) = s_0 + tv$, where $t\geq 0, U(s_0) < 0,$ and $v$ is the robot's constant velocity. To check if the ray $s(t)$ collides with the inflated boundary, we find the first time $t_u$ such that $U(s(t_u)) \geq 0$. This means that $s(t)$ is collision-free for $t \in [0,t_u)$. 
\begin{proposition}
%robot moves with a constant velocity $v$ starting from $s_0$ in the free region, i.e., $U(s_0) < 0$. The 
\label{prop:line_curve_defensive_checking} Consider a ray $s(t) = s_0 + tv$, $t\geq 0$ with $U(s_0) < 0$. Let $x_i^+$ and $x_j^-$ be arbitrary positive and negative support vectors. Then, any point $s(t)$ is free as long as:
\begin{equation}
\label{eq:line_curve_t_condition}
t < t_u := \min_{i \in \{1, \ldots, M^+\}} \rho (s_0, x_i^+, x_j^-)
\end{equation}
where $\scaleMathLine[0.95]{\rho(s_0, x_i^+, x_j^-) = 
\begin{cases} 
      +\infty, & \text{if}\ v^T(x_i^+ - x_j^-) \leq 0  \\
      \frac{\beta - \Vert s_0 - x_j^-\Vert ^2 - \Vert s_0 + x_i^+ \Vert ^2}{2v^T(x_j^- - x_i^+)}, & \text{if}\ v^T(x_i^+ - x_j^-) > 0 
\end{cases},\; \beta = \frac{\left(\log (\alpha_j^-) - \log (\sum_{i = 1} ^ {M^+} \alpha_i^+)\right)}{\gamma}}$.
\end{proposition}
\vspace{-5mm}
\begin{proof} 
We provide a proof sketch here, while the complete proof can be found in Appendix \ref{app:proof_prop_2}.
Based on Proposition \ref{prop:score_bounds}, a point $s(t)$ is free if $U(s(t)) < 0$ or 
\begin{equation}
\label{eq:line_curve_t_condition_x_star}
t < \rho(s_0, x_*^+, x_j^-)
\end{equation}
Since $x_*^+$ varies with $t$ but belongs to a finite set, $U(s(t)) < 0$ if we take the minimum of $\rho (s_0, x_i^+, x_j^-)$ over all positive support vectors.\qedhere
%\begin{equation}
%\label{eq:line_min_t}
%t < t_u = \min_{i \in \{1, \ldots, M^+\}} \rho (s_0, x_i^+, x_j^-)
%\end{equation}
\vspace{-4mm}
\end{proof}
Propositions \ref{prop:score_bounds} and \ref{prop:line_curve_defensive_checking} hold for any negative support vector $x_j^-$. Since $x_j^-$ belongs to a finite set, we can take the best bound on $t$ over the set of negative support vectors as shown in Corollary \ref{collorary:line_curve_tigher_bound}.
\begin{corollary}
\label{collorary:line_curve_tigher_bound} Consider a ray $s(t) = s_0 + tv$, $t\geq 0$ with $U(s_0) < 0$. Let $x_i^+$ and $x_j^-$ be arbitrary positive and negative support vectors, respectively. A point $s(t)$ is free as long as:
\begin{equation}
\label{eq:line_curve_t_condition_tigher_bound}
t <  t_u^* := \min_{i \in \{1, \ldots, M^+\}} \max_{j \in \{1, \ldots, M^-\}} \rho (s_0, x_i^+, x_j^-).
\end{equation}
\vspace{-5mm}
\end{corollary}
The computational complexities of calculating $t_u$ and $t^*_u$ are $O(M)$ and $O(M^2)$, respectively. Depending on the time constraints, one can pick the faster but less accurate bound $t_u$ or the slower but more accurate bound $t^*_u$. Since often the robot's movement is limited to the neighborhood of its current position, $t_u$ can reasonably approximate $t^*_u$ if $x_j^-$ is chosen as the negative support vector, closest to the current location $s_0$. Note also that Eq.~\eqref{eq:line_curve_t_condition} is sufficient but not necessary for Eq.~\eqref{eq:line_curve_t_condition_x_star}, i.e., Eq.~\eqref{eq:line_curve_t_condition} is more conservative than Eq.~\eqref{eq:line_curve_t_condition_x_star}. However, Eq.~\eqref{eq:line_curve_t_condition} is much more efficient in the sense that it has the same complexity as checking a point for collision ($O(M)$), yet it can evaluate the collision status for an entire line segment for $t \in [0, t_u]$.

\begin{figure*}[h!]
\vspace{-3mm}
\begin{minipage}{0.32\linewidth}
\includegraphics[width=\linewidth]{fig/check_line_examples.pdf}
\end{minipage}%
\hfill%
\begin{minipage}{0.66\linewidth}
\vspace{-3ex}
\begin{algorithm}[H]
\caption{Line segment collision check}
\label{alg:collision_checking_line}
\footnotesize
	\begin{algorithmic}[1]	  
		\Require Line segment $(s_A, s_B)$; support vectors $\Lambda^+ = \{(x_i^+, \alpha_i^+)\}$ and $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$
		\Ensure True (Free) or False (Colliding)
		\State $v_A = s_B - s_A$, $v_B = s_A - s_B$
		\State Calculate $t_{uA}$ and $t_{uB}$ using Eq.~\eqref{eq:line_curve_t_condition} or Eq.~\eqref{eq:line_curve_t_condition_tigher_bound}
    \If{$t_{uA} + t_{uB} > 1$}
      \State \Return True
		\Else 
		  \State \Return False
		\EndIf
	\end{algorithmic}
\end{algorithm}
\end{minipage}
\caption{Collision checking for line segments with bounds $t_{uA}$ and $t_{uB}$ obtained from Eq.~\eqref{eq:line_curve_t_condition_tigher_bound}.}
\label{fig:collision_checking_line}
\vspace{-2mm}
\end{figure*}
%\textbf{Line Segments.}
In path planning, one often performs collision checking for a line segment $(s_A, s_B)$. All points on the segment can be expressed as $s(t_A) = s_A + t_Av_A$, $v_A = s_B - s_A$, $0 \leq t_A \leq 1$. Using the upper bound $t_{uA}$ on $t_A$ provided by Eq.~\eqref{eq:line_curve_t_condition} or Eq.~\eqref{eq:line_curve_t_condition_tigher_bound}, we find the free region on the line $AB$ starting from the endpoint $A$. Similarly, we calculate $t_{uB}$ which specifies the free region from endpoint $B$. If $t_{uA} + t_{uB} > 1$, the entire line segment is free; otherwise the segment is considered colliding. The proposed approach is summarized in Algorithm~\ref{alg:collision_checking_line} and illustrated in Fig.~\ref{fig:collision_checking_line}.
%all points on the segment can also be expressed as $s(t_B) = s_B + t_Bv_B$, $v_B = s_A - s_B$, $0 \leq t_B \leq 1$. W
%, and find the free region on the line $AB$ starting from the endpoint $B$
%we consider the segment from endpoint $B$ and calculate an upper bound $t_{uB}$ on $t_B$ using Eq.~\eqref{eq:line_curve_t_condition} or Eq.~\eqref{eq:line_curve_t_condition_tigher_bound}


%\begin{algorithm}
%\caption{Line Segment Collision Check}
%\label{alg:collision_checking_line}
%  \footnotesize
%	\begin{algorithmic}[1]
%		\Require Line segment $(s_A, s_B)$; positive and negative support vectors $\Lambda^+ = \{(x_i^+, \alpha_i^+)\}$, $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$
%%Set of $M^+$ positive support vectors and their weights $\Lambda^+ = \{(x_i^+, \alpha_i^+)\}$; set of $M^-$ positive support vectors and their weights $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$; line segment AB $(s_A, s_B)$.
%		\Ensure True (Free) or False (Colliding)
%		\State $v_A = s_B - s_A$, $v_B = s_A - s_B$
%		\State Calculate $t_{uA}$ and $t_{uB}$ using Eq.~\eqref{eq:line_curve_t_condition} or Eq.~\eqref{eq:line_curve_t_condition_tigher_bound}
%				%\State Calculate $t_{uB}$ using Eq.  \eqref{eq:line_curve_t_condition} or  \eqref{eq:line_curve_t_condition_tigher_bound}
%		\If{$t_{uA} + t_{uB} > 1$} \Return True
%		\Else \;\Return False
%		\EndIf
%	\end{algorithmic}
%\end{algorithm}

%\begin{figure}[t]
%\centering
%\includegraphics[height=2.0in]{fig/check_line_examples.pdf} \hfill
%\includegraphics[height=2.0in]{fig/check_curve_examples.pdf}
%\caption{Collision checking for line segments with the bounds $t_{uA}$ and $t_{uB}$ calculated from Eq.~\eqref{eq:line_curve_t_condition_tigher_bound} (left) and second-order polynomial curves (right).}
%\label{fig:collision_checking}
%\end{figure}


%\begin{figure*}[t]
%\begin{minipage}{0.65\linewidth}
%\includegraphics[width=0.5\linewidth]{fig/check_line_examples.pdf}%
%\hfill%
%\includegraphics[width=0.49\linewidth]{fig/check_curve_examples.pdf}
%\caption{Collision checking for line segments with bounds $t_{uA}$ and $t_{uB}$ obtained from Eq.~\eqref{eq:line_curve_t_condition_tigher_bound} (left) and second-order polynomial curves (right).}
%\label{fig:collision_checking}
%\end{minipage}%
%\hfill%
%\begin{minipage}{0.34\linewidth}
%\vspace{-2ex}
%\begin{algorithm}[H]
%\caption{Line segment collision check}
%\label{alg:collision_checking_line}
%  \footnotesize
%	\begin{algorithmic}[1]
%		\Require Line segment $(s_A, s_B)$; support vectors $\Lambda^+ = \{(x_i^+, \alpha_i^+)\}$ and $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$
%		\Ensure True (Free) or False (Colliding)
%		\State $v_A = s_B - s_A$, $v_B = s_A - s_B$
%		\State Calculate $t_{uA}$ and $t_{uB}$ using Eq.~\eqref{eq:line_curve_t_condition} or Eq.~\eqref{eq:line_curve_t_condition_tigher_bound}
%    \If{$t_{uA} + t_{uB} > 1$} \Return True
%		\Else \;\Return False
%		\EndIf
%	\end{algorithmic}
%\end{algorithm}
%\end{minipage}
%\end{figure*}








\subsubsection{Collision Checking for Polynomial Curves}
\label{subsubsec:collision_check_poly_curve}
% The direction does not change along a line but might vary along a general polynomial curve. 
%consider all directions of $v$ when checking a polynomial curve for collision. Collorary \ref{corollary:free_ball} considers an arbitrary unit velocity vector $v$ (i.e. $\Vert v \Vert = 1$) and find a ball of radius $r$ around $s_0$ whose interior is completely free.
In the previous section, $v$ was a constant velocity representing the direction of motion of the robot. In general, the robot's velocity might be time varying leading to trajectories described by polynomial curves~\cite{liu2017search}. Therefore, we extend the collision checking algorithm to general curves. The key idea is to treat $v$ as an arbitrary unit vector that may point in any direction (i.e.~$\|v\|= 1$) and find a ball of radius $r$ around $s_0$ whose interior is free of obstacles. 


\begin{corollary}
\label{corollary:free_ball} Let $s_0 \in \mathcal{C}$ be such that $U(s_0) <0$ and let $x_i^+$ and $x_j^-$ be arbitrary positive and negative support vectors. Then, every point inside the balls $\mathcal{B}(s_0, r_u) \subseteq \mathcal{B}(s_0, r_u^*)$ is free for:
\begin{equation}
\label{eq:line_curve_radius}
r_u := \min_{i \in \{1, \ldots, M^+\}} \mu(s_0, x_i^+, x_j^-) \qquad\text{and}\qquad r_u^* := \min_{i \in \{1, \ldots, M^+\}} \max_{j \in \{1, \ldots, M^-\}} \mu(s_0, x_i^+, x_j^-)
\end{equation}
where $\mu(s_0, x_i^+, x_j^-) = \frac{\beta - \|s_0 - x_j^-\| ^2 + \|s_0 - x_i^+ \|^2}{2 \| x_j^- - x_i^+ \|}$ and $\beta = \frac{\log (\alpha_j^-) - \log (\sum_{i = 1} ^ {M^+} \alpha_i^+)}{\gamma}$.
\end{corollary}
\vspace{-5mm}
\begin{proof}
The proof is provided in Appendix \ref{subsec:corollary2_proof}.
\vspace{-3mm}
\end{proof}
%\begin{enumerate}
%\item Every point strictly inside the ball $\mathcal{B}(s_0, r)$ is free with
%\begin{equation}
%\label{eq:line_curve_radius}
%r = \min_{i \in \{1, \ldots, M^+\}} r(s_0, x_i^+, x_j^-),
%\end{equation}
%\item Every point strictly inside the ball $\mathcal{B}(s_0, r^*)$ is free with
%\begin{equation}
%\label{eq:line_curve_radius_tighter_bound}
%r^* = \min_{i \in \{1, \ldots, M^+\}} \max_{j \in \{1, \ldots, M^-\}} r(s_0, x_i^+, x_j^-),
%\end{equation}
%\end{enumerate}
%where $r(s_0, x_i^+, x_j^-) = \frac{\beta - \Vert s_0 - x_j^-\Vert ^2 + \Vert s_0 - x_i^+ \Vert ^2}{2 \Vert x_j^- - x_i^+ \Vert}, \beta = \frac{\log (\alpha_j^-) - \log (\sum_{i = 1} ^ {M^+} \alpha_i^+)}{\gamma}$, and $U(s_0) < 0$.
%\end{corollary} 
%\iftrue
%\begin{proof}
%\phantom\qedhere
%\begin{enumerate}
%\item By the Cauchy-Schwarz inequality, $
%v^T (x_j^- - x^+_*)  \leq \Vert v\Vert \Vert (x_j^- - x^+_*)\Vert 
%$. For any unit velocity $v$, Eq. \eqref{eq:line_curve_condition} is satisfied if $t < r(s_0, x_i^*, x_j^-) = \frac{\beta - \Vert s_0 - x_j^-\Vert ^2 + \Vert s_0 - x_*^+ \Vert ^2}{2 \Vert (x_j^- - x^+_*)\Vert}$.
%%\begin{eqnarray}
%%&& t \Vert (x_j^- - x^+_*)\Vert < \frac{\beta - \Vert s_0 - x_j^-\Vert ^2 + \Vert s_0 - x_*^+ \Vert ^2}{2} \nonumber \\
%%&\Leftrightarrow& t < r(s_0, x_i^*, x_j^-) = \frac{\beta - \Vert s_0 - x_j^-\Vert ^2 + \Vert s_0 - x_*^+ \Vert ^2}{2 \Vert (x_j^- - x^+_*)\Vert} \nonumber
%%\end{eqnarray}

%This means any point $s(t)$ strictly inside the ball $\mathcal{B}(s_0, r(s_0, x_i^*, x_j))$ is free. However, this only holds if $x_i^*$ is the closest positive support vector to $s(t)$ which is unknown for an arbitrary $t$. Fortunately, $x_i^*$ belongs to a finite set of positive support vectors. Therefore, any point in the interior of the ball $\mathcal{B}(s_0, r)$ where $r = \min_{i \in \{1, \ldots, M^+\}} r(s_0, x_i^+, x_j^-)$ is free. 
%\item This is because Eq. \eqref{eq:line_curve_radius} holds for any any negative support vector $x_j^-$. 
%\end{enumerate}
%\end{proof}
%\fi
%\textbf{Polynomial Curves.} 
Consider a polynomial $s(t) = s_0 + a_1 t + a_2 t^2 + \ldots + a_d t^d$, $0 \leq t \leq t_f$ of length $L$ from $s_0$ to $s_f := s(t_f)$. Collorary~\ref{corollary:free_ball} shows that all points inside $\mathcal{B}(s_0, r)$ are free. If we can find the smallest positive $t_1$ such that $\|s(t_1) - s_0 \| = r$, we guarantee that all points on the curve $s(t)$ for $0 \leq t \leq t_1$ are free. This is equivalent to finding the smallest non-negative root of a $2d$-order polynomial. We perform collision checking by iteratively covering the curve by Euclidean balls. If the radius of any balls is smaller than a threshold $\varepsilon$, i.e., the ball's center is close to the inflated boundary, the curve is considered colliding. Otherwise, the curve is considered free. In Appendix \ref{subsubsec:check_curves_appendix}, this process is summarized in Algorithm~\ref{alg:collision_checking_curve} and Fig.~\ref{fig:collision_checking_curve} and is shown to terminate in finite time. %Note that, since we check if the radius is smaller than $\varepsilon$, the number of considered balls is bounded by $L/\varepsilon$ and, hence, Algorithm~\ref{alg:collision_checking_curve} terminates in finite time. Fig.~\ref{fig:collision_checking_curve} shows that it only takes a few balls to check if the entire curve is free or colliding.
%If $d \leq 2$, there is a closed-form solution for $t_1$. For higher order polynomials, one can use a root-finding algorithm to obtain $t_1$ numerically. 
% Fig.~\ref{fig:collision_checking} shows two examples of Algorithm~\ref{alg:collision_checking_curve} with radii calculated from Eq.~\eqref{eq:line_curve_radius}. 
%\begin{algorithm}[t]
%\caption{Sequential collision check for a curve $s(t)$.}
%\label{alg:collision_checking_curve}
%  \footnotesize
%	\begin{algorithmic}
%		\Require Curve $s(t)$, $t \in [0,t_f]$; threshold $\varepsilon$; support vectors $\Lambda^+ = \{(x_i^+, \alpha_i^+)\}$ and $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$
%		%Set of $M^+$ positive support vectors and their weight $\Lambda^+ = \{(x_i^+, \alpha_i^+)\}$; set of $M^-$ positive support vectors and their weight $\Lambda^- = \{(x_j^-, \alpha_j^-)\}$, a polynomial curve $s(t), x(0) = s_0, x(1) = s_f, t \in [0,1]$, a threshold $\varepsilon$.
%		\Ensure True (Free) or False (Colliding)
%		\State Set $k = 0$, $t_0 = 0$
%		\While{True}
%			\State Calculate $r_{k}$ using Eq. \eqref{eq:line_curve_radius}
%			\If{$r_k < \varepsilon$} break
%			\EndIf
%			\State Solve $\Vert s(t) - s(t_k) \Vert = r_k$ for $t_{k+1} \geq t_{k}$
%			\If{$t_{k+1} \geq t_f$}
%				\Return True
%			\EndIf
%		\EndWhile
%\State \Return False
%	\end{algorithmic}
%\end{algorithm}



\subsection{Autonomous Navigation}
\label{subsec:auto_nav}
Given the kernel-based map $\hat{m}_t$ proposed in Section \ref{subsec:ogm_with_fastron}, a path planning algorithm such as $A^*$~\cite{Russell_AI_Modern_Approach} may be used with the proposed collision checking algorithms in Section~\ref{subsec:collison_checking_with_fastron_map} to generate a path that solves the autonomous navigation problem (Problem~\ref{problem_formulation_unknown_env}). Algorithm~\ref{alg:get_successors_appendix} in Appendix \ref{subsubsec:get_successor_appendix} provides an example of how our collision checking algorithm may be used to find the successors in $A^*$. For simplicity, we use a fully actuated motion model $x_{t+1} = x_t + u_t$ leading to a piecewise linear robot trajectory. A more precise model (e.g., differential-drive or ackermann-drive for a ground wheeled robot or a quadrotor model as in~\cite{liu2017search}) may be used together with polynomial collision checking (Section \ref{subsubsec:collision_check_poly_curve}). The robot follows the path returned by $A^*$ for some time and updates the map estimate $\hat{m}_t$ with new observations. Using the updated map, the robot re-plans the path and follows the new path instead. This process is repeated until the robot reaches the goal region or a time limit is exceeded. Algorithm~\ref{alg:auto_nav_fastron_ogm} summarizes the proposed autonomous navigation approach.

% using Fastron (Algorithm \ref{alg:fastron_model}) and augmented dataset (Algorithm \ref{alg:augmented_data}) in details.
\vspace{-1mm}
\begin{algorithm}
\caption{Autonomous Mapping and Navigation with Sparse Kernel-based Occupancy Grid Map}
\label{alg:auto_nav_fastron_ogm}
  \footnotesize
	\begin{algorithmic}[1]
		\Require Initial position $x_s \in \mathcal{C}_{free}$; goal region $\mathcal{C}_{goal}$; time limit $T$; prior support vectors $\Lambda_0^+$ and $\Lambda_0^-$
		\State Set $t = 0$, $x_0 = x_s$
		\While {$x_t \notin \mathcal{C}_{goal}$ and $t < T$}
			\State $z_t\gets$ New Depth Sensor Observation
			\State $\mathcal{D}\leftarrow $ Training Data Generation$(z_t, \Lambda_t^+, \Lambda_t^-)$ \Comment{Section~\ref{subsec:ogm_with_fastron} and Appendix~\ref{app:data_gen}}
			\State $\Lambda_{t+1}^+, \Lambda_{t+1}^- \leftarrow $ Fastron$(\Lambda_t^+, \Lambda_t^-, \mathcal{D})$ \Comment{Update the map using Algorithm \ref{alg:fastron_model}}
			\State $\pi_{t} \leftarrow$ Path Planning$(\Lambda_{t+1}^+, \Lambda_{t+1}^-, x_t, \mathcal{X_{\text{goal}}})$ \Comment{Replan path via $A^*$ (Algorithm~\ref{alg:get_successors_appendix} in Appendix \ref{subsec:technical_approach_appendix})}
			\State $u_t = \pi_t(0)$, $x_{t+1} = x_t + u_t$  \Comment{Move to the first position along the path}
		\EndWhile
	\end{algorithmic}
\end{algorithm}



