%, such that $\mathcal{X} = \mathcal{X}_{free} \cup \mathcal{X}_{obs}$ and $\mathcal{X}_{free} \cap \mathcal{X}_{obs} = \emptyset$
\section{Problem Formulation}
\label{sec:problem_formulation}
We consider a spherical robot with center $x \in \mathcal{X}:=[0,1]^d$ and radius $r \in \mathbb{R}_{>0}$ navigating in an unknown environment. Let $\mathcal{X}_{obs}$ and $\mathcal{X}_{free}$ be the obstacle space and free space in $\mathcal{X}$, respectively. Let $\mathcal{B}(p,r)$ be a Euclidean ball of radius $r$ centered at point $p$. In the C-space $\mathcal{C}$, the robot body becomes a point $x$, while the obstacle space and free space are transformed as $\mathcal{C}_{obs} = \cup_{p\in \mathcal{X}_{obs}} \mathcal{B}(p,r)$ and $\mathcal{C}_{free} = \mathcal{X} \setminus \mathcal{C}_{obs}$ so that $\mathcal{C} = \mathcal{C}_{obs} \cup \mathcal{C}_{free} = \mathcal{X}$. Assume that the location $x_t \in \mathcal{C}$ of the robot at time $t$ is known or estimated by a localization algorithm. Given a control signal $u_t \in \mathcal{U}$, the dynamics of the robot are assumed to known and characterized by $x_{t+1} = f(x_t, u_t)$. Taking control input $u_t$ at location $x_t$ also incurs a known motion cost (e.g., distance travelled or energy used) denoted as $c(x_t, u_t)$. At time $t$, the robot receives a partial observation $z_t$ of the obstacle space $\mathcal{X}_{obs}$ from its onboard sensors according to an observation model: $z_t = h(x_t, \mathcal{X}_{obs})$. Assume $z_t$ is a vector of distances returned by a depth sensor at time $t$. In unknown environments, we consider constructing a map $\hat{m}_t: \mathcal{C} \rightarrow \{-1, 1\}$ of the C-space based on accumulated observation $z_{0:t}$, where ``-1" and ``1" mean ``free" and ``occupied", respectively.  Assuming that unobserved regions of the estimated map $\hat{m}_t$ are free, we can use $\hat{m}_t$ to plan a motion trajectory for the robot to navigate to a goal region $\mathcal{C}_{goal} \subset \mathcal{C}_{free}$. As the robot navigates, new sensor data is used to update the map and recompute the motion plan. In this online setting, the map update, $\hat{m}_{t+1} = g(\hat{m}_t, z_t)$, is a function of the previous estimate $\hat{m}_t$ and a newly received observation $z_t$. The autonomous mapping and navigation problem is formulated below.

\begin{problem}
\label{problem_formulation_unknown_env}
Given a start state $x_s \in \mathcal{C}_{free}$ and a goal region $\mathcal{C}_{goal} \subset \mathcal{C}_{free}$, find a sequence of control inputs that leads the robot to the goal region safely, while minimizing the motion cost:
\begin{equation}
\label{problem_formulation_unknown_env_equation}
\begin{aligned}
\min_{T, u_0, \ldots, u_T} \;&\sum_{t=0}^T  c(x_t, u_t)\\
\text{s.t.} \quad\;\; &x_{t+1} = f(x_t, u_t), \;\;&&x_0 = x_s, \;\;&&x_T \in \mathcal{C}_{goal}, \quad &t& = 0,\ldots, T,\\
&z_t = h(x_t,\mathcal{X}_{obs}), \;\; &&\hat{m}_{t+1} = g(\hat{m}_{t}, z_t),\;\;&&\hat{m}_t(x_t) = - 1, \quad &t&= 0,\ldots,T.
\end{aligned}
\end{equation}
\end{problem}


%\begin{eqnarray}
%    \min_{T, u_0, \ldots, u_T} && {\sum_{t=0}^T c(x_t, u_t)} \label{problem_formulation_unknown_env_equation}\\
%    \text{s.t.} \quad&& x_{t+1} = f(x_t, u_t), \;\;x_0 = x_s, \;\;x_T \in \mathcal{C}_{goal}, \quad &t = 0,\ldots, T, \nonumber \\
%    &&z_t = h(x_t,\mathcal{X}_{obs}), \;\; \hat{m}_{t+1} = g(\hat{m}_{t}, z_t),\;\;\hat{m}_t(x_t) = - 1, \quad &t = 0,\ldots,T.\nonumber
%    %&&\hat{m}_t(x_t) = - 1, \quad t = 0,\ldots, T. \nonumber
%    %&&x_t \in \hat{\mathcal{C}}^t_{free} := \{x: \hat{m}_t(x) = -1\}, \quad t = 0,\ldots, T. \nonumber
%\end{eqnarray}


%The function $g$ characterizes the map learning process which involves generating data in C-space from the current observation $z_t$ and updating the \TODO{kernel perceptron model}\NA{I recommend presenting a concise overview of our approach instead of a general discussion. Instead of using "the kernel perceptron model" or a "common approach to solve Problem 1", the tone should present what we contribute, e.g., } with the newly generated data. These steps will be described in details in Section \ref{subsec:ogm_with_fastron}. Given a map estimation $\hat{m}_t$, a common approach to solve Problem \ref{problem_formulation_unknown_env} is to find an optimal path by deploying a path planning algorithm such as $A^*$. Such path planning algorithms require collision checking algorithms for our map estimation which is developed for linear and polynomial trajectories in Section \ref{subsec:collison_checking_with_fastron_map}. Finally, we combine the map learning process and its collision checking algorithm into an autonomous mapping and navigation framework in Section \ref{subsec:auto_nav} to solve Problem \ref{problem_formulation_unknown_env}.


% In this section, we formulate the problem of autonomous mapping and navigation of a disk-shaped robot with center $x$ and radius $r \in \mathbb{R}^+$ in an unknown environment. 
% that specifies the next state $x_{t+1}$.
%\begin{equation}
%x_{t+1} = f(x_t, u_t). \label{robot_dynamics}
%\end{equation}

%Note that we define $z_t$ in the work space and will convert it to C-space in Section \ref{sec:technical_approach}.
%The observation $z_t$ at time $t$ is defined as a set of data points $(p,q)$ where $p$ is a state in $\mathcal{X}$ and label $q \in \{-1, 1\}$ indicates whether $p$ is free ("-1") or occupied ("1"). 

%The unobserved regions are assumed to be free. The map estimation $\hat{m}_t$ is then used to find an optimal path to the goal region. The robot follows this path, collects new information on the environment, updates its map estimation and its optimal path on the fly. In this online setting, the map estimation $\hat{m}_t = g(\hat{m}_{t-1}, z_t)$ is a function of the new observation $z_t$ and the previous map estimation $\hat{m}_{t-1}$. 
